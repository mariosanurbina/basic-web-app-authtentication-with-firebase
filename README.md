# Basic web app authtentication with Firebase

Basic static page with authentication based on Firebase Auth. You just need to update the credentials in the index.js file.  With just a browser without the need of a server, you could see Firebase in action. 
For further explanation please read this little tutorial in Medium: https://medium.com/@Mariosanurbina/basic-web-app-authentication-with-firebase-8c997e3ef2f5