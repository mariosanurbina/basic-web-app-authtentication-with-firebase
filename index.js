 // Your web app's Firebase configuration
 (function(){

  const firebaseConfig = {
    apiKey: /*put your credentials hear*/,
    authDomain: /*put your credentials hear*/,
    databaseURL: /*put your credentials hear*/,
    projectId: /*put your credentials hear*/,
    storageBucket: "",
    messagingSenderId: /*put your credentials hear*/,
    appId: /*put your credentials hear*/,
    measurementId: /*put your credentials hear*/
  };

  firebase.initializeApp(firebaseConfig);

  //Get elements
  const txtEmail = document.getElementById('txtEmail');
  const txtPassword = document.getElementById('txtPassword');
  const btnLogin = document.getElementById('btnLogin');
  const btnSignUp = document.getElementById('btnSignUp');
  const btnLogout = document.getElementById('btnLogout');


  //Add login event
  btnLogin.addEventListener('click', () =>{
    // Get email and password
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();

    //Sign in
    const promise = auth.signInWithEmailAndPassword(email,pass);

    promise

    .catch(errors => console.log(errors.message)); //errors

  });

  //Add sign up button
  btnSignUp.addEventListener('click',()=>{
    // Get email and password
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();
    console.log("antes de la promesa");

    //Sign in
    const promise = auth.createUserWithEmailAndPassword(email,pass);
    promise
    .catch(errors => console.log(errors.message)); //errors

  });

  btnLogout.addEventListener('click',() => {
    firebase.auth().signOut();
  });


  // Add real time listener
  firebase.auth().onAuthStateChanged(firebaseUser => {
    if (firebaseUser){


      console.log(firebaseUser);

    } else {
      console.log('No se ha autenticado');

    }
  });


 }());
